export const exibirLogo = (path) => {
  const img = document.createElement('img');

  img.src = path;
  img.className = 'logo_img';

  return img;

}