import { createPopper } from '@popperjs/core';
import './style.css';
import { verificarDados } from './verificarDados'
import { exibirLogo } from './view/exibeLogo';
import { trocarFundo } from './view/trocaFundo';

document.addEventListener('DOMContentLoaded', () => {
    loadComponents();
});

const loadComponents = () => {
    const app = document.getElementById('app');
    
    app.innerHTML = `
    <div id="card" class="w-50 ajustarAoConteudo row">
        <div clss="col-12">
            <form>
                <div>
                    <input type="text" class="form-control" id="nome" placeholder="Digite seu nome">
                </div>
                <div class="mt-3">
                    <label for="time" class="form-label">Escolha seu time</label>
                    <select  id="times" class="form-select texto">
                        <option value="c">Corinthians</option>
                        <option value="p">Palmeiras</option>
                        <option value="sp">São Paulo</option>
                        <option value="s">Santos</option>
                    </select>
                </div>
            <div>
            <div class="mt-3"
                 <button type="submit" class="btn btn-primary texto">exibir</button>
            </div>
        </form>
        <div id="msg" class="mt-3"</div>
    </div>
    <div id="logo" class="col-12 mt-3">
    </div>
 </div>
`;

const inputNome = document.getElementById('nome');
const selectTime = document.getElementById('times');
const btnExibir = document.querySelector('[type= "submit"');
const msgDiv = document.getElementById('msg');
const logoDiv = document.getElementById('logo');

bntExibir.addEventListener('click', (event) => {
    event.preventDefault();

    const resposta = verificarDados(
        inputNome.value,
        selectTime.value 
    );
})

}

const loadTeams = (componentMsg, componentLogo, data) =>{
    removeContent(componentLogo);
    removeContent(componentMsg);

    if (data.status !== 0) {
        componentMsg.innerHTML = `
        <div class="alert alert-warning alert-dismissble fade show" role="alert">
            ${data.message}
            <button type="button" class="btn-close" data-bs-dismiss+"alert" aria-label="Close"></button>
            </div>
    `;
    return;
    }

    componentMsg.innerHTML = data.message;

    trocarFundo(`assets/fundos/${data.fundo}`);

    componentLogo.appendChild(
        exibirLogo(`assents/escudos/${data.logo}`)
    )
}

const removeContent = (component) => {
    component.innerHTML = '';
}